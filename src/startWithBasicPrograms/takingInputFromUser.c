/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 * 
 * This file is part of learn-basic-c.
 * Learn-basic-c is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or any
 * later version, as specified in the readme.md file.
 */

#include <stdio.h>

int
main ()
{
  /* integer values */
  int num1, num2;
  /* decimal value */
  float fraction;
  /* character value */
  char character;

  /* printf() is used to display text onto the screen. */
  printf ("Enter two numbers number\n");

  /*
   * Taking integer as input from user
   *
   * scanf() is used to take input from the user using format
   *         specifier discussed in upcoming tutorials.
   *
   * %d and %i both are used to take numbers an input from the user.
   */
  scanf ("%d%i", &num1, &num2);
  printf ("The two numbers you have entered are %d and %i\n", num1, num2);

  printf ("Enter a Decimal number\n");
  /*
   * %f is the format specifier to take float as input
   * %g to keep the number without postfix 0, like 1.20000
   */
  scanf ("%f", &fraction);
  printf ("The float that you have entered is %g\n", fraction);

  printf ("Enter a Character\n");
  /*
   * Here is a problem, the scanf take the <Enter> as the input
   * Use getchar() to deal this.
   */
  getchar();
  /* %c is the format specifier to take character as input */
  scanf ("%c", &character);
  printf ("The character that you have entered is %c\n", character);

  return 0;
}