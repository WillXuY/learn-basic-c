/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 * 
 * This file is part of learn-basic-c.
 * Learn-basic-c is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or any
 * later version, as specified in the readme.md file.
 */

#include <stdio.h>

int
main ()
{
  /* %c is the format specifier to take character as input */
  char c;
  printf ("Enter a character: ");
  scanf ("%c", &c);
  printf ("\nASCII value of %c = %d", c, c);

  return 0;
}
