# I - ⚖️ License

Copyright (C) 2021  Weiyang(Will) Xu

This file is part of learn-basic-c.
Learn-basic-c is free software: you can redistribute it and/or modify
it under the term of the GNU General Public License version 3 or any
later version, as specified in the readme.md file.

### 0 - Install the Utils (WSL)
- Operating system: WSL install guide (todo)
  1. Search "Ubuntu" in the Microsoft store.
  2. Install the most popular one.
  3. 

### 1 - Quick Start (for complete newbie)
1. Add a directory in home directory, named learnC. (ps. don't copy the '$' to command)
``` bash
$ mkdir ~/learnC
```
2. Move into the learnC (All the project will be in it)
``` bash
$ cd ~/learnC/
```
3. Add a directory for Start with Basic Programs and cd in it.
``` bash
$ mkdir startWithBasicPrograms/
$ cd startWithBasicPrograms/
```
- Tips: press \<TAB\> can auto replenish the directory name.
- Tips: check where you are: ~/learnC/startWithBasicPrograms
``` bash
$ pwd
```
- /home/'Your_name' is the '~' directory in unix like system. in terminal it will looks like:
``` bash
'Your_name'@'Your_computer_name':~/learnC/startWithBasicPrograms$ pwd
/home/'Your_name'/learnC/startWithBasicPrograms
```
