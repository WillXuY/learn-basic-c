/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 * 
 * This file is part of learn-basic-c.
 * Learn-basic-c is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or any
 * later version, as specified in the readme.md file.
 */

#include <stdio.h>

int
main ()
{
  char grade;
  printf ("Enter your grade:\n");
  scanf ("%c", &grade);

  /* switch() can only contain char and int. */
  switch (grade)
    {
      /*
       * A char variable is always initialized within
       * ''(single quotes).
       */
      case 'A':
        printf ("Excellent\n");
        /* 
         * break is used to exit from switch statement.
         * If there is no break statement then the cases following the
         * matched case other than default will get executed.
         */
        break;
      case 'B':
        printf ("Keep it up!\n");
        break;
      case 'C':
        printf ("Well done\n");
        break;
      case 'D':
        printf ("You passed\n");
        break;
      case 'E':
        printf ("Better luck next time\n");
        break;
      default:
        printf ("Invalid grade\n");
    }
  
  printf ("Your gradle is %c\n", grade);

  return 0;
}
