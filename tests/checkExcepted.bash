#!/bin/bash

# Copyright (C) 2021  Weiyang(Will) Xu
# 
# This file is part of learn-basic-c.
# Learn-basic-c is free software: you can redistribute it and/or modify
# it under the term of the GNU General Public License version 3 or any
# later version, as specified in the readme.md file.

readonly EXCEPT_TYPE='.except'
readonly ACTUAL_TYPE='.actual'
readonly DIFF_TYPE='.diff'

function checkExcepted()
{
  # BELLOW CODE: Compare diff will ALWAYS be the same.
  # Diff compare the difference.
  except_file=$TEMPORARY_NAME$EXCEPT_TYPE
  actual_file=$TEMPORARY_NAME$ACTUAL_TYPE
  diff_file=$TEMPORARY_NAME$DIFF_TYPE

  echo "$except" > $except_file
  echo "$actual" > $actual_file
  $(diff $except_file $actual_file > $diff_file)

  if [ ! -s $diff_file ]; then
    rm $TEMPORARY_NAME.*
  else
    echo -e "\033[1;33mOutput not match!\033[0m"
    # Output the full information.
    echo "See the diff except and actual result file at:"
    echo -e "\033[35m$diff_file\033[0m"
    echo -e "\033[32m$except_file\033[0m"
    echo -e "\033[31m$actual_file\033[0m"
    echo "See the source file at:"
    echo -e "\033[36m$source_file\033[0m"
    echo "Run the output file use:"
    echo -e "\033[36m./$out_file\033[0m"
    echo -e "\033[1;35mDiff info:\033[0m"
    cat $diff_file
  fi
}
