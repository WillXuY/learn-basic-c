#!/bin/bash

# Copyright (C) 2021  Weiyang(Will) Xu
# 
# This file is part of learn-basic-c.
# Learn-basic-c is free software: you can redistribute it and/or modify
# it under the term of the GNU General Public License version 3 or any
# later version, as specified in the readme.md file.

source ../checkExcepted.bash

# Change this url and name
readonly SOURCE_DIR='../../src/startWithBasicPrograms/'
readonly PROJECT_NAME='ifElse'
# Files with this name will be delete when test run successful.
readonly TEMPORARY_NAME="temporary.$PROJECT_NAME"

source_file=$SOURCE_DIR$PROJECT_NAME.c
out_file=$TEMPORARY_NAME.out

# sed -i 's/\r//' <file_name> to fix problem that file name has \r
# the wrong file name looks like <file_name>\r
gcc $source_file -o $out_file

# Provide the except result here.
except='Please enter a number:
This number is less than 100!'
actual=`echo 19 | ./$out_file`
# check the except and actual in ../checkExcepted.bash
checkExcepted

gcc $source_file -o $out_file
except='Please enter a number:
This number is 100!'
actual=`echo 100 | ./$out_file`
checkExcepted

gcc $source_file -o $out_file
except='Please enter a number:
This number is greater than 100!'
actual=`echo 190 | ./$out_file`
checkExcepted
