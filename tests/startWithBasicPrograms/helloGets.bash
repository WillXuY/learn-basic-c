#!/bin/bash

# Copyright (C) 2021  Weiyang(Will) Xu
# 
# This file is part of learn-basic-c.
# Learn-basic-c is free software: you can redistribute it and/or modify
# it under the term of the GNU General Public License version 3 or any
# later version, as specified in the readme.md file.

source ../checkExcepted.bash

# Some of the important points about scanf() and gets() are:
# scanf() and gets() both are used to take input from the user.
# scanf() can only take input until it encounters a space. 
#         the words after space are ignored by it.
# gets() is used to take a single input at a time but can be used to
#        input a complete sentence with spaces unlike scanf().

# Change this url and name
readonly SOURCE_DIR='../../src/startWithBasicPrograms/'
readonly PROJECT_NAME='helloGets'
# Files with this name will be delete when test run successful.
readonly TEMPORARY_NAME="temporary.$PROJECT_NAME"

source_file=$SOURCE_DIR$PROJECT_NAME.c
out_file=$TEMPORARY_NAME.out

# sed -i 's/\r//' <file_name> to fix problem that file name has \r
# the wrong file name looks like <file_name>\r
gcc $source_file -o $out_file

echo "Enter your complete name: "
read str

# Provide the except result here.
except="Enter your complete name:
Welcome, $str"

actual=`echo $str | ./$out_file`

# check the except and actual in ../checkExcepted.bash
checkExcepted
