#!/bin/bash

# Copyright (C) 2021  Weiyang(Will) Xu
# 
# This file is part of learn-basic-c.
# Learn-basic-c is free software: you can redistribute it and/or modify
# it under the term of the GNU General Public License version 3 or any
# later version, as specified in the readme.md file.

source ../checkExcepted.bash

# Program to take input of various data types in C.

# It is a program to explain how to take input from user for different
# data types available in C language.
# The different data types are int(integer values),
# float(decimal values) and char(character values).

# Here is the C language tutorial explaining various data types:
# https://www.studytonight.com/c/datatype-in-c.php

# Change this url and name
readonly SOURCE_DIR='../../src/startWithBasicPrograms/'
readonly PROJECT_NAME='takingInputFromUser'
# Files with this name will be delete when test run successful.
readonly TEMPORARY_NAME="temporary.$PROJECT_NAME"

source_file=$SOURCE_DIR$PROJECT_NAME.c
out_file=$TEMPORARY_NAME.out

# sed -i 's/\r//' <file_name> to fix problem that file name has \r
# the wrong file name looks like <file_name>\r
gcc $source_file -o $out_file

# Get input from user input.
# IMPORTANT bash will read any variable input, but C cannot.
# SO, if input a invalid data, it will be accept by the bash.
# But won't be accept by the C, the result will be different.
echo "Enter two numbers number"
read num1
read num2
echo "The two numbers you have entered are $num1 and $num2"

echo "Enter a Decimal number"
read fraction
echo "The float that you have entered is $fraction"

echo "Enter a Character"
read character
echo "The character that you have entered is $character"

# Provide the except result here.
except="Enter two numbers number
The two numbers you have entered are $num1 and $num2
Enter a Decimal number
The float that you have entered is $fraction
Enter a Character
The character that you have entered is $character"

actual=`echo "$num1 $num2 $fraction $character" | ./$out_file`

# check the except and actual in ../checkExcepted.bash
checkExcepted
